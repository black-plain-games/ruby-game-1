module Services
    class ResourceService
        def initialize
            @resources = {}
        end

        def get(key)
            resources[key]
        end

        def set(key, value)
            resources[key] = value
        end
        
        def remove(key)
            resources.delete(key)
        end

        def clear
            resources.clear
        end
        
        private

        attr_reader :resources
    end
end