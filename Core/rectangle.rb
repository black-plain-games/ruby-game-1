require 'ruby2d'

require_relative "point.rb"
require_relative "dirty.rb"

module Core
    class Rectangle
        attr_reader :position
        attr_reader :visible

        def initialize(width, height, color)
            @rectangle = Ruby2D::Rectangle.new(width: width, height: height, color: color)  

            @position = Core::Point.new
            @visible = Core::Dirty.new(value: true)
            @visible.value_changed_handler = -> (caller, previous, current) {
                if current
                    @rectangle.add
                else
                    @rectangle.remove
                end
            }
        end

        def update
            @rectangle.x = @position.x
            @rectangle.y = @position.y
        end
    end
end