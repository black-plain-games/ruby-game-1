require 'ruby2d'

module Services
    class InputService
        def initialize
            @key_states = {}

            Ruby2D::Window.on :key_down do |event|
                @key_states[event.key] = :down
            end

            Ruby2D::Window.on :key_up do |event|
                @key_states[event.key] = :up
            end
        end

        def down?(key)
            return false unless @key_states.has_key? key
            @key_states[key] == :down
        end

        def up?(key)
            return false unless @key_states.has_key? key
            @key_states[key] == :up
        end
    end
end