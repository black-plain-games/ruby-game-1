module Services
    class GameService
        def initialize(state_service:)
            @state_service = state_service
        end

        def setup(title, initial_state)
            state_service.push(initial_state)

            {
                window: {
                    width: 640,
                    height: 480,
                    title: "Sample Game 100"
                }
            }
        end
        
        def update
            state_service.peek.update
        end

        private
        
        attr_reader :state_service
    end
end