require_relative "Utilities/service_locator.rb"
require_relative "Services/logging_service.rb"
require_relative "Services/game_service.rb"
require_relative "Services/input_service.rb"
require_relative "Services/resource_service.rb"
require_relative "Services/state_service.rb"
require_relative "States/test_state.rb"

class Package
    def self.registrations
        {
            logging_service: Services::LoggingService,
            input_service: Services::InputService,
            resource_service: Services::ResourceService,
            state_service: Services::StateService,
            initial_state: {
                type: States::TestState,
                params: [:logging_service, :input_service, :resource_service]
            },
            game_service: {
                type: Services::GameService,
                params: [:state_service]
            }
        }
    end
end