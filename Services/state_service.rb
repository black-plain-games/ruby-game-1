module Services
    class StateService
        def initialize
            @states = []            
        end

        def push(state)
            peek.suspend unless states.empty?
            state.load
            states << state
        end

        def pop
            s = states.last
            states.delete(s)
            s.unload
            peek.resume unless states.emty?
            s
        end

        def peek
            states.last
        end

        def clear
            states.clear
        end

        private
        attr_reader :states
    end
end