module Core
    class Dirty    
        attr_accessor :value_changed_handler
            
        def initialize(value:)
            @previous_value = value
            @value = value
        end
        
        def set_value(value)
            return if @value == value
            @previous_value = @value
            @value = value
            return if value_changed_handler.nil?
            value_changed_handler.call(self, @previous_value, @value)
        end

        def get_value
            @value
        end
    end
end