module Utilities
    class ServiceLocator
        def initialize
            @resolvers = {}
            @resolutions = {}
            register(:services, Utilities::ServiceLocator)
        end

        def register(key, type, *params)
            resolvers[key] = {
                type: type,
                params: params
            }
        end

        def register_package(package)
            package.registrations.each do |key, value|
                if value.class == Hash
                    resolvers[key] = {
                        type: value[:type],
                        params: value[:params]
                    }
                else
                    resolvers[key] = {
                        type: value,
                        params: []
                    }
                end
            end
        end

        def resolve(key)
            return resolutions[key] if resolutions.has_key? key

            resolver = resolvers[key]

            params = {}

            resolver[:params].each do |p|
                params[p] = resolve(p)
            end

            resolutions[key] = resolver[:type].new(**params)
        end

        private

        attr_reader :resolvers
        attr_reader :resolutions
    end
end