# External requires
require 'ruby2d'

# Internal requires
require_relative "package.rb"

# Infra

services = Utilities::ServiceLocator.new

services.register_package(Package)

# Setup

initial_state = services.resolve(:initial_state)

game = services.resolve(:game_service)

setup_data = game.setup("Sample Game", initial_state)

set(setup_data[:window])

next_time = Time.now

update { game.update }

# Showtime

show