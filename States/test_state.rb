require_relative "base_state.rb"
require_relative "../Core/rectangle.rb"

module States
    class TestState < BaseState
        def initialize(logging_service:, input_service:, resource_service:)
            @logging_service = logging_service
            @input_service = input_service
            @resources = resource_service

            @updateables = []
        end

        def load
            r = Core::Rectangle.new(100, 50, 'blue')
            updateables << r
            resources.set(:recty, r)
        end

        def unload
        end

        def suspend
        end

        def resume
        end
        
        def update
            speed = 2

            r = resources.get(:recty)
            
            r.position.x -= speed if input_service.down? "a"
            r.position.x += speed if input_service.down? "d"
            r.position.y -= speed if input_service.down? "w"
            r.position.y += speed if input_service.down? "s"
            
            updateables.each do |value|
                value.update
            end
        end

        private

        attr_reader :resources
        attr_reader :updateables
        attr_reader :input_service
    end
end