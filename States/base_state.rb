module States
    class BaseState
        def load
        end

        def unload
        end

        def suspend
        end

        def resume
        end
        
        def update
        end
    end
end